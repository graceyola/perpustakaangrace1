<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

// Route::get('about', function () {
//     return view('about');
// });

// Route::get('mahasiswa', function () {
//     return view('mahasiswa');
// });

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');


Route::get('/perpustakaan', 'PerpustakaanController@index');
Route::get('/perpustakaan/create', 'PerpustakaanController@create');
Route::post('/perpustakaan', 'PerpustakaanController@store');
Route::get('/perpustakaan/edit/{id}', 'PerpustakaanController@edit');
Route::post('/perpustakaan/update', 'PerpustakaanController@update'); 
Route::get('/delete/{id}', 'PerpustakaanController@delete'); 

Route::get('/dafta', 'PerpustakaanController@index'); 






  