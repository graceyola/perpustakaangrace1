@extends('layout/main');

@section('title', 'Form ')


@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-2">Form Data Perpustakaan</h1>
                    <form method="post" action="/perpustakaan/">
                    @csrf
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Buku" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="kode">Kode</label>
                            <input type="text" class="form-control" id="kode" placeholder="Masukkan Kode Buku" name="kode">
                        </div>
                        <div class="form-group">
                            <label for="tahun">Tahun</label>
                            <input type="tahun" class="form-control" id="tahun" placeholder="Masukkan tahun terbit Buku" name="tahun">
                        </div>
                        <button type="submit" class="btn btn-primary">Tambah Data </button>
                        
                    </form>

            </div>
           
        </div>
    </div>
@endsection

    