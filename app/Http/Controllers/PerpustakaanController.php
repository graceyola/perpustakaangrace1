<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\dafta;
use App\Tag;

class PerpustakaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$daftar = DB::table('daftar')->get();
        $daftar =\App\dafta::all();
        $daftar = DB::table('daftar')->paginate(5);

        
        

        return view('perpustakaan.index', ['daftar'=>$daftar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perpustakaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dft = new dafta;
        $dft->nama = $request->nama;
        $dft->kode = $request->kode;
        $dft->tahun = $request->tahun;

        $dft->save();

        return redirect('/perpustakaan')->with('status','Data buku berhasil ditambahkan'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Respo nse
     */
    public function edit($id)
    {
        $daftar = DB::table('daftar')->where('id',$id)->get();
        return view('perpustakaan.edit', ['daftar'=> $daftar]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('daftar')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'kode' => $request->kode
            ]);

          

           
        
            return redirect('/perpustakaan');
        
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
    public function delete($id)
    {
        DB::table('daftar')->where('id', $id)->delete();

        return redirect('/perpustakaan');
    }
}
